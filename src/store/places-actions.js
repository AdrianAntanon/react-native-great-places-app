import * as FileSystem from 'expo-file-system';
import { fetchPlaces, insertPlace } from '../helpers/db';

export const ADD_PLACE = 'ADD_PLACE';
export const SET_PLACES = 'SET_PLACES';

export const addPlace = (title, image) => {
  return async dispatch => {
    // Nos recoge la url de la foto, la dividimos en un array y extraemos la última posición
    // Por ejemplo adri/antanyon/adri.jpg, por lo que nos quedaríamos con el nombre de la imagen que es adri.jpg
    const fileName = image.split('/').pop();

    // Accedemos a la ruta de archivos guardados, en concreto a la imagen guardada.
    const newPath = FileSystem.documentDirectory + fileName;

    // Para mover el archivo accedo a los archivos y le digo que voy a mover el archivo de A a B, uso una promesa porque puede tardar un poco en hacer.
    // Meto todo dentro de un trycatch por que puede fallar, ya sea por espacio insuficiente en el móvil o no tenga permisos para realizar la acció<n className=""></n>
    try {
      await FileSystem.moveAsync({
        from: image,
        to: newPath
      });
      // Guardamos nuestra foto en SQLite, es una promesa por lo que hacemos await 
      const dbResult = await insertPlace(
        title,
        newPath,
        'Dummy address',
        15.6,
        12.3
      );
      dispatch({ type: ADD_PLACE, placeData: { id: dbResult.insertId, title: title, image: newPath } });
    } catch (error) {
      console.log(error);
      throw error;
    }

  }
};

export const loadPlaces = () => {
  return async dispatch => {
    try {
      const dbResult = await fetchPlaces();
      dispatch({ type: SET_PLACES, places: dbResult.rows._array })
    } catch (err) {
      throw err;
    }
  };
};